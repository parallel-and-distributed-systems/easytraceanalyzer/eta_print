#include <eta/core/debug.hpp>
#include <eta/trace/loader.hpp>
#include <eta/core/ProgramOptions.hxx>

bool interleave_events=false;
bool verbose=false;
struct container_status  {
  const eta::Trace& t;
  eta::Container* cont;
  std::vector<eta::EventId>::iterator it;
};

void printEvent(const eta::Trace &t, const eta::Event &e) {
  std::cout.precision(15);

  std::cout<<"["<<nlc::rm_unit(e.timestamp)<<"] ["<<e.container_id.value()<<"] ";
  bool printDetails = false;
  if(verbose)
    printDetails=true;
  if(e.type == eta::EventType::EnterFunction) {
    std::optional<eta::Value> &&fname = t.get_field(e, "FunctionName");
    std::cout<<"Enter ("<< fname<<") ";
    
  } else if (e.type == eta::EventType::ExitFunction) {
    std::optional<eta::Value> &&fname = t.get_field(e, "FunctionName");
    std::cout<<"Leave ("<< fname<<")";
  } else {
    printDetails=true;
  }

  if(printDetails) {
    std::cout<<e.type<<" { ";
    switch(e.type) {
    case eta::EventType::CreateContainer: break;
    case eta::EventType::DestroyContainer: break;
    case eta::EventType::SendMessage:
      break;
    case eta::EventType::ReceiveMessage:
      break;
    case eta::EventType::EnterFunction:
      break;
    case eta::EventType::IoOperationBegin:
      break;
    case eta::EventType::IoOperationComplete:
      break;
    case eta::EventType::ExitFunction:
      break;
    case eta::EventType::MpiOperationBegin:
      break;
    case eta::EventType::MpiOperationComplete:
      break;
    case eta::EventType::Other: break;
    default:
      Assert(false, "Unknown event type: ", e.type);
    }

    for(const auto &field:e.fields) {
      std::cout<<t.fields.get_field_name(field.id)<<"='"<<field.value<<"', ";
    }
    std::cout<<" }";
  }
  std::cout<<std::endl;
}

auto get_next_event(std::vector<struct container_status> &containers)
  -> struct container_status&
{
  eta::Date min_ts = std::numeric_limits<eta::Date::underlying>::max();
  std::vector<struct container_status>::iterator min_i = containers.end() ;

  for(std::vector<struct container_status>::iterator i =  containers.begin();
      i != containers.end();
      ++i) {

    if(i->it != i->cont->events.end()) {
      const eta::Event &e = i->t.event(*i->it);
      if(e.timestamp < min_ts) {
	min_ts = e.timestamp;
	min_i = i;
      }
    }
  }

  return *min_i;
}

void printContainers(std::vector<struct container_status> &containers) {
  int nb_remaining_containers = containers.size();
  while(nb_remaining_containers > 0) {
    struct container_status &c = get_next_event(containers);
    printEvent(c.t, c.t.event(*c.it));
    c.it++;

    if(c.it == c.cont->events.end())
      nb_remaining_containers--;
  }

}


void printContainer(const eta::Trace &t, const eta::Container &cont) {
  std::cout<<"\tContainer "<<cont.id<<" : "<<cont.name<<" : "<<cont.events.size()<<" events"<<std::endl;
  for(const auto &event:cont.events) {
    printEvent(t, t.event(event));
  }
}

void printTrace(const eta::Trace& t) {
  std::cout<<"Trace info:"<<std::endl;
  std::cout<<"\t.name: "<<t.name<<std::endl;
  std::cout<<"\t.path: "<<t.path<<std::endl;
  std::cout<<"\t.type: "<<t.type<<std::endl;
  std::cout<<"\t.events: "<<t.events.size()<<std::endl;
  std::cout<<"\t.containers: "<<t.containers.size()<<std::endl;
  std::cout<<"\t.communications: "<<t.communications.size()<<std::endl;

  for(const auto& cont:t.containers ) {
    printContainer(t, cont);
  }
}

int main(int argc, char**argv) {
  po::parser programOptions;

  programOptions["help"].abbreviation('h').single().description("print this help screen");

  programOptions["verbose"]
    .abbreviation('v')
    .description("enables extra informations")
    .callback([]() {
                eta::set_log_level(eta::log_level_t::debug);
                eta::log_info("set debug level to 'debug'");
	      });


  programOptions["interleave"]
    .abbreviation('i')
    .description("Interleave events from all the containers");

  std::vector<std::string> files;
  programOptions[""].bind(files);

  if (!programOptions(argc, argv)) {
    eta::log_critical("Error occured during cli arguments parsing. Aborting");
    return EXIT_FAILURE;
  }

  if (programOptions["help"].size() > 0) {
    std::cout << programOptions << '\n';
    return EXIT_SUCCESS;
  }

  if (programOptions["interleave"].size() > 0) {
    interleave_events=true;
  }

  if (programOptions["verbose"].size() > 0) {
    verbose=true;
  }

  if(interleave_events) {
    std::vector<struct container_status> containers;
    std::vector<eta::Trace> traces;

    for (auto &trace_file: files) {
      std::cout<<"Opening "<<trace_file<<std::endl;
      auto trace = eta::loadTrace(eta::Path(trace_file));
      if(std::holds_alternative<eta::ParsingError>(trace)) {
	std::cerr<<"Error parsing "<<trace_file<<std::endl;
	return EXIT_FAILURE;
      }

      traces.push_back(std::move(std::get<eta::Trace>(trace)));
    }

    for (auto &t: traces) {
      for(auto& cont:t.containers ) {
	if(cont.events.size() > 0) {
	  containers.push_back(container_status{t, &cont, cont.events.begin()});
	}
      }
    }

    printContainers(containers);
  } else {
    for (auto const &trace_file: files) {
      std::cout<<"Opening "<<trace_file<<std::endl;
      auto trace = eta::loadTrace(eta::Path(trace_file));
      if(std::holds_alternative<eta::ParsingError>(trace)) {
	auto e = std::get<eta::ParsingError>(trace);
	std::cerr<<"Error parsing: "<<e<<std::endl;
	return EXIT_FAILURE;
      }

      auto& t = std::get<eta::Trace>(trace);
      printTrace(t);
    }
  }

  return EXIT_SUCCESS;
}
